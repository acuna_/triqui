class Triqui {

    constructor() {

        this.matriz = [
            ["A", "B", "C"],
            ["D", "E", "F"],
            ["G", "H", "I"]
        ];
        this.dibujarTablero();
        this.definirJugador();


    }

    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML +
                "<input type='text' id='casilla" + (i + 1) + "' class='casilla' onclick='miTriqui.realizarJugada()'>";

            if ((i + 1) % 3 == 0) {
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }

        }
    }


    definirJugador() {
        let n;

        let miTurno = document.getElementById("turno");
        n = Math.round(Math.random() + 1);

        if (n === 1) {
            this.turno = "X";
        } else {
            this.turno = "O";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    realizarJugada() {

        let miElemento = event.target;

        if (!(miElemento.value === "X" || miElemento.value === "O")) {
            miElemento.value = this.turno;
            this.modificarMatriz(miElemento.id);

            let resultado=this.verificarTriqui();
     
            if(resultado===true){

                document.getElementById("respuesta").innerHTML = "TRIQUI!!!! Gano: "+ this.turno;

            }else{
                this.cambiarTurno();
            }
            
        } else {
            document.getElementById("respuesta").innerHTML = "la casilla está llena";
            
        }

    }


    verificarTriqui() {

        let triqui = document.getElementById("respuesta").innerHTML = false;
            
        

        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][0]=== this.matriz[fila][2]) {
                triqui = true;
                return triqui;
            }
        }

        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[0][columna]=== this.matriz[2][columna]) {
                triqui = true;
                return triqui;
            }
        }

        if (this.matriz[0][0] === this.matriz[1][1] && this.matriz[0][0]=== this.matriz[2][2]) {
            triqui = true;
            return triqui;
        }

        if (this.matriz[0][2] === this.matriz[1][1] && this.matriz[0][2] === this.matriz[2][0]) {
            triqui = true;
            return triqui;
        }


        return triqui;
    }

    


    modificarMatriz(id) {

        switch (id) {

            case "casilla1":
                this.matriz[0][0] = this.turno;
                break;
            case "casilla2":
                this.matriz[0][1] = this.turno;
                break;
            case "casilla3":
                this.matriz[0][2] = this.turno;
                break;
            case "casilla4":
                this.matriz[1][0] = this.turno;
                break;
            case "casilla5":
                this.matriz[1][1] = this.turno;
                break;
            case "casilla6":
                this.matriz[1][2] = this.turno;
                break;
            case "casilla7":
                this.matriz[2][0] = this.turno;
                break;
            case "casilla8":
                this.matriz[2][1] = this.turno;
                break;
            case "casilla9":
                this.matriz[2][2] = this.turno;
                break;
        }
    }



    cambiarTurno() {

        let miTurno = document.getElementById("turno");

        if (this.turno === "X") {
            this.turno = "O";
        } else {
            this.turno = "X";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;

    }

}